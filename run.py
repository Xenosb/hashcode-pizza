def main():
    (n_rows, n_cols, min_ing, max_ing, pizza, flags) = read_from_cmd()
    #print_pizza(n_rows, n_cols, pizza)
    #print find_rectangle_dimens(24)
    find_closest_slice(0, 0, pizza, flags, min_ing, max_ing)
    print_pizza(n_rows, n_cols, pizza, flags)


# Reads input data from cmd and returns a pizza, max and min ingredients
def read_from_cmd():
    pizza = {}
    flags = {}

    (a, b, c, d) = raw_input().split(' ')
    (n_rows, n_cols, min_ing, max_ing) = (int(a), int(b), int(c), int(d))

    for row in range(n_rows):
        row_in = raw_input()
        for col in range(n_cols):
            pizza[(row, col)] = row_in[col]
            flags[(row, col)] = 0

    return (n_rows, n_cols, min_ing, max_ing, pizza, flags)


# 3D prints you a delicious pizza
def print_pizza(n_rows, n_cols, pizza, flags):
    pizza_out = ''
    flags_out = ''
    print ''
    for row in range(n_rows):
        for col in range(n_cols):
            pizza_out += pizza[(row,col)]
            flags_out += str(flags[(row,col)])
        pizza_out += '\n'
        flags_out += '\n'
    print pizza_out
    print flags_out


# Paints closest slice to given coord taken
def find_closest_slice(row, col, pizza, flags, min_ing, max_ing):
    for slice_size in range(min_ing*2, max_ing):
        for (slice_rows, slice_cols) in find_rectangle_dimens(slice_size):

            #print 'Size: {}, combination ({}, {})'.format(slice_size, slice_rows, slice_cols)
            # TODO do if instead of try except
            try:
                tomas = 0
                mushs = 0
                for test_row in range(slice_rows):
                    for test_col in range(slice_cols):
                        #print '({} {}) = {}'.format(row + test_row, col + test_col, pizza[row + test_row, col + test_col][0])
                        if pizza[row + test_row, col + test_col] == 'T':
                            tomas += 1
                        else:
                            mushs += 1
                #print 'M: {}, T:{}'.format(mushs, tomas)
                if tomas >= min_ing and mushs >= min_ing:
                    for flag_row in range(row, row + slice_rows):
                        for flag_col in range(col, col + slice_cols):
                            flags[row + flag_row, col + flag_col] = 1
                    return ((row, col), (row + slice_rows - 1, col + slice_cols - 1))
            except:
                pass
            #print '{} {}'.format(slice_rows, slice_cols)


# Returns list of possible rectangle dimensions for max size
def find_rectangle_dimens(n):
    res = []
    fact = reduce(list.__add__, ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0))
    for i in range(len(fact)/2):
        res += [(fact[i*2], fact[i*2+1])]
        res += [(fact[i*2+1], fact[i*2])]
    return set(res)


if __name__ == "__main__":
    main()
